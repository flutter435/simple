import 'package:dio/dio.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simple/state/dio_provider.dart';

final userDataProvider = StateProvider((ref) => []);
final postDataProvider = StateProvider((ref) => []);
final userProvider = Provider((ref) => UserService(ref: ref));

class UserService {
  final Ref ref;
  UserService({required this.ref});

  Future<bool> loadData() async {
    try {
      final dio = ref.read(dioProvider);
      final res = await dio.get("https://jsonplaceholder.typicode.com/users");
      ref.read(userDataProvider.notifier).state = res.data;
    } on DioError catch (e) {
      return false;
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<bool> loadPostData() async {
    try {
      final dio = ref.read(dioProvider);
      final res = await dio.get("https://jsonplaceholder.typicode.com/posts");
      ref.read(postDataProvider.notifier).state = res.data;
    } on DioError catch (e) {
      return false;
    } catch (e) {
      return false;
    }
    return true;
  }
}
