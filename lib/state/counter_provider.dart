import 'package:hooks_riverpod/hooks_riverpod.dart';

final counterProvider = StateProvider((_) => 0);
