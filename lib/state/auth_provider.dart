import 'package:firebase_auth/firebase_auth.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

enum AuthState {
  unAuth,
  logedIn,
}

final authProvider = StateProvider((ref) => AuthState.unAuth);
final loginProvider = Provider(
  (ref) => {
    FirebaseAuth.instance.authStateChanges().listen((User? user) {
      if (user != null) {
        ref.read(authProvider.notifier).state = AuthState.logedIn;
      } else {
        ref.read(authProvider.notifier).state = AuthState.unAuth;
      }
    })
  },
);
