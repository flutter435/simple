import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simple/state/counter_provider.dart';
import 'package:simple/widget/counter_button.dart';
import 'package:simple/widget/counter_display.dart';

class MyHomePage extends HookConsumerWidget {
  final String title;
  MyHomePage({required this.title});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
      ),
      body: Container(
        margin: EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Center(
              child: CounterDisplay(),
            ),
            CounterButton(),
            ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/tab');
              },
              child: Text("To to tab demo"),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          ref.read(counterProvider.notifier).state += 1;
        },
        child: Icon(Icons.add_circle),
      ),
    );
  }
}

// class MyHomePage extends StatefulWidget {
//   final String title;
//   MyHomePage({required this.title});

//   @override
//   State<StatefulWidget> createState() {
//     return _MyHomeState();
//   }
// }

// class _MyHomeState extends State<MyHomePage> {
//   int num = 0;
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(widget.title),
//       ),
//       body: Column(
//         children: [
//           Text("$num"),
//           ElevatedButton(
//             onPressed: () {
//               setState(() {
//                 num = num + 1;
//               });
//             },
//             child: Text("Add"),
//           ),
//         ],
//       ),
//     );
//   }
// }
