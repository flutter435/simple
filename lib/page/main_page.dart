import 'package:flutter/src/widgets/framework.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simple/page/login_page.dart';
import 'package:simple/page/my_home_page.dart';
import 'package:simple/state/auth_provider.dart';

import 'home_page.dart';

class MainPage extends HookConsumerWidget {
  const MainPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.watch(loginProvider);
    final auth = ref.watch(authProvider);
    if (auth == AuthState.unAuth) {
      return LoginPage();
    } else {
      return HomePage();
    }
  }
}
