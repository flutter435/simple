import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:simple/widget/counter_button.dart';
import 'package:simple/widget/counter_display.dart';

class TabBarDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Demo"),
          bottom: TabBar(tabs: [
            Tab(
              icon: Icon(Icons.directions_car),
            ),
            Tab(
              icon: Icon(Icons.directions_bike),
            ),
            Tab(
              icon: Icon(Icons.directions_boat),
              child: CounterDisplay(),
            )
          ]),
        ),
        body: TabBarView(
          children: [
            CounterDisplay(),
            CounterButton(),
            Icon(Icons.directions_boat),
          ],
        ),
      ),
    );
  }
}
