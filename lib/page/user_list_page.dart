import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simple/state/user_provider.dart';
import 'package:simple/widget/row_space.dart';

class UserListPage extends HookConsumerWidget {
  const UserListPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    useEffect((() {
      ref.read(userProvider).loadData();
    }), []);

    return Container(
      margin: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          searchButton(ref),
          const RowSpace(),
          Expanded(child: listData(ref))
        ],
      ),
    );
  }

  Widget searchButton(WidgetRef ref) {
    return ElevatedButton(
      onPressed: () {
        ref.read(userProvider).loadData();
      },
      child: Text("Search"),
    );
  }

  Widget listData(WidgetRef ref) {
    final data = ref.watch(userDataProvider);
    return ListView.builder(
      itemCount: data.length,
      itemBuilder: ((context, index) {
        final d = data.elementAt(index);
        return ListTile(
          title: Text("${d['name']}"),
          subtitle: Text("${d['email']}"),
          leading: Icon(Icons.person),
        );
      }),
    );
  }
}
