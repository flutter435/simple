import 'package:flutter/material.dart';

import '../widget/row_space.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  bool isShowPassword = false;

  @override
  Widget build(Object context) {
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.all(25),
        child: Column(
          children: [
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  emailInput(),
                  const RowSpace(),
                  passwordInput(),
                  const RowSpace(),
                  registerButton(),
                  const RowSpace(),
                  registerLink(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget registerButton() {
    return ElevatedButton(
      onPressed: () {
        if (_formKey.currentState!.validate()) {
          // TODO: if validate is true
        } else {
          // TODO: if validate false
        }
      },
      child: const Text("Login"),
    );
  }

  TextFormField emailInput() {
    return TextFormField(
      decoration: const InputDecoration(
        // icon: Icon(Icons.email),
        labelText: 'Email',
        hintText: 'Please input email.',
        border: OutlineInputBorder(),
        prefixIcon: Icon(Icons.email),
        suffixIcon: Icon(Icons.check),
      ),
      keyboardType: TextInputType.emailAddress,
      validator: ((value) {
        if (value!.isEmpty) {
          return "Email is empty";
        }
        if (value.length < 5) {
          return "Email < 5";
        }
        return null;
      }),
    );
  }

  TextFormField passwordInput() {
    return TextFormField(
      decoration: InputDecoration(
          // icon: Icon(Icons.email),
          labelText: 'Password',
          hintText: 'Please input password.',
          border: const OutlineInputBorder(),
          prefixIcon: const Icon(Icons.lock),
          isDense: true,
          suffix: InkWell(
            child: Icon(
              isShowPassword ? Icons.visibility : Icons.visibility_off,
            ),
            onTap: () {
              setState(() {
                isShowPassword = !isShowPassword;
              });
            },
          )),
      obscureText: !isShowPassword,
      validator: ((value) {
        if (value!.isEmpty) {
          return "Password is empty";
        }
        if (value.length < 8) {
          return "Password < 8";
        }
        return null;
      }),
    );
  }

  Widget registerLink() {
    return TextButton(
        onPressed: () {
          Navigator.pushNamed(context, '/register');
        },
        child: const Text("Create new user"));
  }
}
