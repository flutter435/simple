import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:simple/widget/row_space.dart';

import '../widget/date_input_field.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _RegisterState();
  }
}

class _RegisterState extends State<RegisterPage> {
  final _formKey = GlobalKey<FormState>();
  final _email = TextEditingController();
  final _password = TextEditingController();
  int _sex = 0;
  bool _accept = false;
  final _passenger = ['1', '2', '3', '4', '5'];
  String _person = '1';
  final _dateInput = TextEditingController();
  Uint8List? _img;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Register"),
      ),
      body: Container(
        margin: const EdgeInsets.all(20),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              emailInput(),
              const RowSpace(),
              passwordInput(),
              const RowSpace(),
              radioInput(),
              const RowSpace(),
              acceptCheck(),
              const RowSpace(),
              passengetDLL(),
              const RowSpace(),
              DateInputField(
                dateInput: _dateInput,
              ),
              const RowSpace(),
              getImage(),
              const RowSpace(),
              registerButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget emailInput() {
    return TextFormField(
      decoration: const InputDecoration(labelText: "Email"),
      controller: _email,
    );
  }

  Widget passwordInput() {
    return TextFormField(
      decoration: const InputDecoration(labelText: "Password"),
      controller: _password,
      obscureText: true,
    );
  }

  Widget registerButton() {
    return ElevatedButton(
      onPressed: () async {
        if (_formKey.currentState!.validate()) {
          try {
            final resp =
                await FirebaseAuth.instance.createUserWithEmailAndPassword(
              email: _email.text,
              password: _password.text,
            );
            Navigator.pop(context);
          } on FirebaseAuthException catch (e) {
            if (e.code == 'weak-password') {
              showWarning("weak password");
            } else if (e.code == 'email-already-in-use') {
              showWarning("email already in use");
            }
          } catch (e) {
            showWarning(e.toString());
          }
        } else {
          showWarning("Please validate user input");
        }
      },
      child: const Text("Register"),
    );
  }

  showWarning(String message) {
    showDialog(
      context: context,
      builder: ((context) {
        return AlertDialog(
          title: Text("Warning"),
          content: Text(message),
        );
      }),
    );
  }

  Widget radioInput() {
    return Row(
      children: [
        const Text("Sex"),
        Radio(
            value: 0,
            groupValue: _sex,
            onChanged: (value) {
              setState(() {
                _sex = value ?? 0;
              });
            }),
        const Text("Male"),
        Radio(
            value: 1,
            groupValue: _sex,
            onChanged: (value) {
              setState(() {
                _sex = value ?? 0;
              });
            }),
        const Text("Female"),
      ],
    );
  }

  Widget acceptCheck() {
    return Row(
      children: [
        Checkbox(
          value: _accept,
          onChanged: ((value) {
            setState(() {
              _accept = value ?? false;
            });
          }),
        ),
        const Text("I agree ......"),
      ],
    );
  }

  Widget passengetDLL() {
    return DropdownButtonFormField(
      decoration: const InputDecoration(
        labelText: 'Passenger',
      ),
      items: _passenger.map((String value) {
        return DropdownMenuItem(
          value: value,
          child: Text(value),
        );
      }).toList(),
      onChanged: (String? value) {
        _person = value ?? '1';
      },
      value: _person,
    );
  }

  Widget getImage() {
    if (_img == null) {
      return ElevatedButton(
        onPressed: () async {
          final img = await ImagePicker.platform.pickImage(
            source: ImageSource.camera,
          );
          _img = await img!.readAsBytes();
          setState(() {});
        },
        child: const Text("Choose Image"),
      );
    } else {
      return Image.memory(_img!);
    }
  }
}
