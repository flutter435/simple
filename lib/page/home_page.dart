import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simple/page/my_home_page.dart';
import 'package:simple/page/post_list_page.dart';
import 'package:simple/page/user_list_page.dart';

import '../state/home_provider.dart';

class HomePage extends HookConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: getBodyByIndex(ref.watch(homeIndexProvider)),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: ref.watch(homeIndexProvider),
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
          BottomNavigationBarItem(icon: Icon(Icons.newspaper), label: "News"),
          BottomNavigationBarItem(icon: Icon(Icons.settings), label: "Setting"),
        ],
        onTap: ((value) {
          ref.read(homeIndexProvider.notifier).state = value;
        }),
      ),
    );
  }

  Widget getBodyByIndex(int index) {
    switch (index) {
      case 0:
        return MyHomePage(title: "My");
      case 1:
        return const UserListPage();
      case 2:
        return const PostListPage();
      default:
        return Text("");
    }
  }
}
