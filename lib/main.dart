import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simple/firebase_options.dart';
import 'package:simple/page/main_page.dart';
import 'package:simple/page/register_page.dart';
import 'package:simple/page/tab_bar_demo.dart';

void main() async {
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(
    const ProviderScope(
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: MyHomePage(title: 'Flutter Demo Home Page'),
      // home: TabBarDemo(),
      initialRoute: '/',
      routes: {
        '/': (context) => const MainPage(),
        '/tab': (context) => TabBarDemo(),
        '/register': (context) => const RegisterPage(),
      },
    );
  }
}

// class MyHomePage extends StatelessWidget {
//   int num = 0;
//   final String title;
//   MyHomePage({required this.title});

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(title),
//       ),
//       body: Column(
//         children: [
//           Text("$num"),
//           ElevatedButton(
//             onPressed: () {
//               num = num + 1;
//               print(num);
//             },
//             child: Text("Add"),
//           ),
//         ],
//       ),
//     );
//   }
// }

