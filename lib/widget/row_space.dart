import 'package:flutter/widgets.dart';

class RowSpace extends StatelessWidget {
  const RowSpace({super.key});

  @override
  Widget build(BuildContext context) {
    return const SizedBox(
      height: 8,
    );
  }
}
