import 'package:flutter/material.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:simple/state/counter_provider.dart';

class CounterDisplay extends HookConsumerWidget {
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    int num = ref.watch(counterProvider);
    return Text("$num");
  }
}
