import 'package:flutter/material.dart';

class DateInputField extends StatefulWidget {
  final TextEditingController dateInput;
  const DateInputField({super.key, required this.dateInput});

  @override
  State<StatefulWidget> createState() {
    return _DateInputState();
  }
}

class _DateInputState extends State<DateInputField> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          // width: 100,
          child: TextFormField(
            controller: widget.dateInput,
          ),
        ),
        SizedBox(
          width: 32,
          child: IconButton(
            onPressed: () async {
              DateTime? _date = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime.now(),
                lastDate: DateTime.now().add(
                  const Duration(days: 30),
                ),
              );
              widget.dateInput.text =
                  "${_date!.day}/${_date.month}/${_date.year}";
            },
            icon: Icon(Icons.calendar_today),
          ),
        ),
      ],
    );
  }
}
